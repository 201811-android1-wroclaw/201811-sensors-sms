package pl.jsystems.android.sensory201811;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.widget.Toast;

public class PermissionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int readSmsResult = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS);
        int receiveSmsResult = ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS);
        int sendSmsResult = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);

        if (readSmsResult != PackageManager.PERMISSION_GRANTED
                || receiveSmsResult != PackageManager.PERMISSION_GRANTED
                || sendSmsResult != PackageManager.PERMISSION_GRANTED) {
            // TODO
            requestPermissions(new String[]{
                    Manifest.permission.READ_SMS,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.SEND_SMS
            }, 123);
        } else {
            sendTestSms();
            finish();
        }
    }

    private void sendTestSms() {
        SmsManager smsManager = SmsManager.getDefault();
        String content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur";
        String number = "666196900";

        smsManager.sendMultipartTextMessage(number, null,
                smsManager.divideMessage(content), null, null);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Mówi się trudno!", Toast.LENGTH_SHORT).show();
                break;
            }
        }
        finish();
    }
}
