package pl.jsystems.android.sensory201811;

import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity implements SensorEventListener {
    private View tvSensor;
    private SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvSensor = findViewById(R.id.tv_sensor);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        List<Sensor> sensorList = sensorManager.getSensorList(Sensor.TYPE_ALL);
        System.out.println(sensorList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this,
                sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // x,y,z
        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int screenHeight = getResources().getDisplayMetrics().heightPixels;

        double xMargin = (screenWidth / 2.0) - ((screenWidth / 2.0) * (sensorEvent.values[0] / 10.0));

        double yMargin = (screenHeight / 2.0) + ((screenHeight / 2.0) * (sensorEvent.values[1] / 10.0));

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) tvSensor.getLayoutParams();
        params.topMargin = (int) yMargin;
        params.leftMargin = (int) xMargin;
        tvSensor.setLayoutParams(params);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
