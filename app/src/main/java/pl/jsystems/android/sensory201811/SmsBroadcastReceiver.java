package pl.jsystems.android.sensory201811;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;

public class SmsBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Object[] pdus = (Object[]) intent.getExtras().get("pdus");
        for (Object pdu : pdus) {
            SmsMessage message = SmsMessage.createFromPdu((byte[]) pdu);
            Log.d("SmsBroadcastReceiver", String.format(
                    "SMS z %s, o treści %s",
                    message.getOriginatingAddress(),
                    message.getMessageBody()
            ));
        }
    }
}
